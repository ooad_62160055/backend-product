const express = require('express')
const router = express.Router()
const Product = require('../model/Product')
// const products = [
//   { id: 1, name: 'IPad gen 1 64G Wifi', price: 11000 },
//   { id: 2, name: 'IPad gen 2 64G Wifi', price: 12000 },
//   { id: 3, name: 'IPad gen 3 64G Wifi', price: 13000 },
//   { id: 4, name: 'IPad gen 4 64G Wifi', price: 14000 },
//   { id: 5, name: 'IPad gen 5 64G Wifi', price: 15000 },
//   { id: 6, name: 'IPad gen 6 64G Wifi', price: 16000 },
//   { id: 7, name: 'IPad gen 7 64G Wifi', price: 17000 },
//   { id: 8, name: 'IPad gen 8 64G Wifi', price: 18000 },
//   { id: 9, name: 'IPad gen 9 64G Wifi', price: 19000 },
//   { id: 10, name: 'IPad gen 10 64G Wifi', price: 20000 }
// ]

// const lastId = 11

router.get('/', async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.get('/:id', async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found!!'
      })
    }
    res.json(product)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
})

router.post('/', async function (req, res, next) {
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.put('/:id', async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
  // const product = {
  //   id: productId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }

  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })

  // if (index >= 0) {
  //   products[index] = product
  //   res.json(products[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'no product id'
  //   })
  // }
})

router.delete('/:id', async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })

  // if (index >= 0) {
  //   products.splice(index, 1)
  //   res.status(204).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'no product id'
  //   })
  // }
})

module.exports = router
