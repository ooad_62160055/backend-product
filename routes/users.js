const express = require('express')
const router = express.Router()
const User = require('../model/User')

router.get('/', async function (req, res, next) {
  try {
    const users = await User.find({}).exec()
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.get('/:id', async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(user)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
})

router.post('/', async function (req, res, next) {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.put('/:id', async function (req, res, next) {
  const UserId = req.params.id
  try {
    const user = await User.findById(UserId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
})

router.delete('/:id', async function (req, res, next) {
  const UserId = req.params.id
  try {
    await User.findByIdAndDelete(UserId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
})

module.exports = router
