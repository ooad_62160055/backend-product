const mongoose = require('mongoose')
const User = require('../model/User')
mongoose.connect('mongodb://localhost:27017/example')
const { ROLE } = require('../constant.js')

async function clearUser () {
  await User.deleteMany({})
}

const main = async function () {
  await clearUser()
  const user = new User({ username: 'user@mail.com', password: 'password', roles: [ROLE.USER] })
  user.save()

  const admin = new User({ username: 'admin@mail.com', password: 'password', roles: [ROLE.ADMIN, ROLE.USER] })
  admin.save()
}

main().then(function () {
  console.log('Finish')
})
