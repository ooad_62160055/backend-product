const mongoose = require('mongoose')
const Event = require('../model/event')
mongoose.connect('mongodb://localhost:27017/example')

async function clearEvent () {
  await Event.deleteMany({})
}

const main = async function () {
  await clearEvent()
  await Event.insertMany([
    {
      title: 'Title 1', content: 'Content 1', startDate: new Date('2022-03-20 08:00'), endDate: new Date('2022-03-20 10:00'), class: 'a'
    },
    {
      title: 'Title 2', content: 'Content 2', startDate: new Date('2022-03-30 08:00'), endDate: new Date('2022-03-30 16:00'), class: 'b'
    },
    {
      title: 'Title 3', content: 'Content 3', startDate: new Date('2022-03-25 10:00'), endDate: new Date('2022-03-25 16:00'), class: 'a'
    }
  ])
}

main().then(function () {
  console.log('Finish')
})
